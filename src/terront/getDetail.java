/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terront;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kulz0
 */
public class getDetail {
    public static void main(String[] args) {
        try {
            String fileName = "3.jpg";
            showDetail(new File(fileName));
        } catch (IOException ex) {
            Logger.getLogger(getDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void showDetail(File file) throws IOException {
        String md5;
        try (InputStream is = new FileInputStream(file)) {
            md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
            System.out.println("MD5: " + md5);
            System.out.println("File Size: " + file.length());
            is.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(getDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
