package terront;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends Thread{
    public static int downloaded=0;
    /**
     * @param args the command line arguments
     */
    
    public void run() {
        ServerSocket listener = null;
        try {
            listener = new ServerSocket(12345);
            System.out.printf("Waiting for connection on port %d ...",listener.getLocalPort());
        } catch (IOException e){
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, e);
        }
        while(true){
            try {
                Socket newSock = listener.accept();
                Thread serverServe = new ServerServe(newSock);
                serverServe.start();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
    }
}

class ServerServe extends Thread{
    private Socket newSock;
    public ServerServe(Socket connSock) {
        this.newSock = connSock;
   }
    public void run()  {
        try {
            BufferedReader reader;
            BufferedWriter writer;
            String fileName;
            char[] buff = new char[16*1024];
            int buffSize;
            System.out.printf("\nConnected(%s:%d)\n",newSock.getInetAddress(),newSock.getPort());
            reader = new BufferedReader(new InputStreamReader(newSock.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(newSock.getOutputStream()));
            buffSize = reader.read(buff);
            if(new String(buff,0,5).equalsIgnoreCase("@100:")){
                fileName = new String(buff,5,buffSize-5);
                String md5 = "ERROR";
//                try (InputStream is = new FileInputStream(new File(fileName))) {
//                    md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
//                    is.close();
//                } catch(FileNotFoundException ex){
//                    buff = new String("@404:").toCharArray();
//                    writer.write(buff, 0, buff.length);
//                    writer.flush();
//                }
                long fileSize = new File(fileName).length();
                buff = new String("@200:" + "f87245772c6cbc19f7ddb157ec216edc" + '|' + fileSize).toCharArray();
                writer.write(buff, 0, buff.length);
                writer.flush();
                while(true){
                    buff = new char[16*1024];
                    buffSize = reader.read(buff);
                    if(new String(buff,0,5).equalsIgnoreCase("@101:")){
                        String temp[] = new String(buff,5,buffSize-5).split("\\|");
                        long offset = Long.parseLong(temp[0]);
                        long count = Long.parseLong(temp[1]);
                        sendFile(newSock, new File(fileName), offset, count);
                    }
                    else if(new String(buff,0,5).equalsIgnoreCase("@END:")){
                        System.out.println("\nServer Received @END: from " + newSock.getInetAddress());
                        writer.write("@CLOSED:", 0, 8);
                        writer.flush();
                        break;
                    }
                }
            }
            else{
                //Not a req
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            try {
                newSock.close();
                System.out.printf("\nClosed %s:%d\n",newSock.getInetAddress(),newSock.getPort());
            } catch (IOException ex) {
                Logger.getLogger(ServerServe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static long sendFile(Socket sockConn, File fts){
        RandomAccessFile freader = null;
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            freader = new RandomAccessFile(fts, "r");
            byte buff[] = new byte[16*1024];
            long fileSize = fts.length();
            long buffSize = 16*1024;
            long fremain = fileSize;
            long foffset = 0;
            long sendB = buffSize-21;
            int responseSize;
            long start = System.currentTimeMillis();
            while(fremain>0){
                if(fremain<=sendB){
                    sendB=fremain;
                }
                buff = new byte[16*1024];
                freader.seek(foffset);
                freader.read(buff,21,Integer.parseInt(String.valueOf(sendB)));
                arraysFill(offsetString(foffset).getBytes(),buff, 0, 21);
                dos.write(buff, 0, Integer.parseInt(String.valueOf(sendB))+21);
                dos.flush();
                buff = new byte[16*1024];
                responseSize = dis.read(buff);
                if(new String(buff).equalsIgnoreCase("@END:")){
                    break;
                }
                else if(bytesToLong(buff)>fileSize){
                    break;
                }
                else if(foffset==bytesToLong(buff)){
                    foffset += sendB;
                    fremain -= sendB;
                }
                System.out.printf("\r%.2f %%",(float)foffset*100.0/(float)fileSize);
            }
            long duration = System.currentTimeMillis()-start;
            if(duration<1000){
                System.out.print(" (" + duration + " ms)");
            }
            else{
                System.out.printf(" (%.2f s)", (float)duration/1000.0);
            }
            return foffset;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                freader.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }
    
    private static long sendFile(Socket sockConn, File fts, long offset, long count){
        RandomAccessFile freader = null;
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            freader = new RandomAccessFile(fts, "r");
            byte buff[] = new byte[16*1024];
            long fileSize = fts.length();
            long buffSize = 16*1024;
            long fremain = count;
            long foffset = offset;
            long sendB = buffSize-21;
            int responseSize;
//            long start = System.currentTimeMillis();
            while(fremain>0){
                if(fremain<=sendB){
                    sendB=fremain;
                }
                buff = new byte[16*1024];
                freader.seek(foffset);
                freader.read(buff,21,Integer.parseInt(String.valueOf(sendB)));
                arraysFill(offsetString(foffset).getBytes(),buff, 0, 21);
                dos.write(buff, 0, Integer.parseInt(String.valueOf(sendB))+21);
                dos.flush();
                buff = new byte[16*1024];
                responseSize = dis.read(buff);
                if(bytesToLong(buff)>fileSize){
                    break;
                }
                else if(foffset==bytesToLong(buff)){
                    foffset += sendB;
                    fremain -= sendB;
                }
            }
//            long duration = System.currentTimeMillis()-start;
//            if(duration<1000){
//                System.out.printf(" (" + duration + " ms)");
//            }
//            else{
//                System.out.printf(" (%.2f s)", (float)duration/1000.0);
//            }
            freader.close();
            return foffset;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                freader.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }
    
    private static String offsetString(long foffset){
        String temp = String.valueOf(foffset);
        String output = "1";
        for(int i = 0; i < 20-temp.length();i++){
            output += "0";
        }
        output+=temp;
        return output;
    }
    
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.put(bytes);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
    
    private static void arraysFill(byte[] src, byte[] des, int offset, int length){
        for(int i=offset;i<offset+length;i++){
            des[i] = src[i];
        }
    }
}

