package terront;


/*
 * To change threader license header, chowritere License Headers in Project Properties.
 * To change threader template file, chowritere Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import static terront.Download.longToBytes;

/**
 *
 * @author hiiamlala
 */

public class Client extends Thread{
    public void run(){
        String[] IPList = {"127.0.0.1","123.26.16.22","55.10.2.1"};
        String fileName = "2.exe";
        String filemd5 = "f87245772c6cbc19f7ddb157ec216edc";
        long fileSize = 410865392;
        Download down = new Download(IPList, fileName, filemd5, fileSize);
        down.start();
    }
}

class Download extends Thread{
    public String[] IPList;
    public long fileSize;
    public String fileName,filemd5;
    public ArrayList<DownloadPiece> PieceList = new ArrayList<>();
    public ArrayList<speedTest> speedList = new ArrayList<>();
    public int piececount = 0;
    public Work work;

    public Download(String[] IPList, String fileName, String filemd5, long fileSize) {
        this.IPList = IPList;
        this.fileName = fileName;
        this.filemd5 = filemd5;
        this.fileSize = fileSize;
        this.work = new Work(fileName, fileSize, filemd5, generateDownloadList(fileSize,IPList.length));
    }
    
    public void run(){
        int i = 0;
        long fileSizeChecked=0;
        long start = System.currentTimeMillis();
        for(String element : IPList){
            System.out.println("Start thread with IP: " + element);
            PieceList.add(new DownloadPiece(element, fileName, filemd5, fileSize, work));
            speedList.add(new speedTest(PieceList.get(piececount)));
            PieceList.get(piececount).start();
            speedList.get(piececount).start();
            piececount++;
        }
        while(!work.isAllDone()){
            try {
                sleep(500);
            } catch (InterruptedException ex) {
                //Logger.getLogger(Download.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        long duration = System.currentTimeMillis() - start;
        if(duration<1000){
            System.out.print("\nDowload in: (" + duration + " ms)\n");
        }
        else{
            System.out.printf("Download in: (%.2f s)\n", (float)duration/1000.0);
        }
        mergeFile(fileName);
        duration = System.currentTimeMillis() - start;
        if(duration<1000){
            System.out.print("\nTotal: (" + duration + " ms)\n");
        }
        else{
            System.out.printf("Total: (%.2f s)\n", (float)duration/1000.0);
        }
    }
    
    public static boolean mergeFile(String fileName){
        try {
            File dir = new File(fileName.split("\\.")[0]);
            File[] list = dir.listFiles();
            File output = new File(dir, fileName);
            output.createNewFile();
            RandomAccessFile fwriter = new RandomAccessFile(output, "rw");
            for(File ele : list){
                if(ele.getName().contains("CAPSLOCKTEAM")){
                    String[] detail = ele.getName().split("\\-");
                    if(detail[0].equalsIgnoreCase(fileName.split("\\.")[0])){
                        long offset = Long.valueOf(detail[detail.length - 3]);
                        long count = Long.valueOf(detail[detail.length - 2]);
                        byte[] buff = new byte[(int)count];
                        FileInputStream reader = new FileInputStream(ele);
                        reader.read(buff,0,(int)count);
                        fwriter.seek(offset);
                        fwriter.write(buff);
                        reader.close();
                        ele.delete();
                    }
                }
            }
            fwriter.close();
            return true;
        } catch (IOException ex) {
            System.out.println("Error when save file: " + fileName);
            return false;
        }
    }
    
    public static ArrayList<Piece> generateDownloadList(long fileSize, int hostNumber){
        ArrayList<Piece> output = new ArrayList<>();
        if(hostNumber <= 1){
            output.add(new Piece(0, fileSize));
        }
        else{
            int devide;
            if(fileSize > (int) Math.pow(hostNumber, 2)){
                devide = (int) Math.pow(hostNumber, 2);
            }
            else devide = hostNumber;
            long interval = fileSize/devide;
            for(int i = 0; i<devide; i++){
                if(i==devide-1){
                    output.add(new Piece(i*interval, fileSize - i*interval));
                }
                else    output.add(new Piece(i*interval, interval));
                
            }
        }
        return output;
    }
    
    public long fileChecker(String fileName, String filemd5, Socket socketOfClient) {
        BufferedWriter writer;
        BufferedReader reader;
        char buff[] = new char[16*1024];
        int buffSize;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
            writer.write("@100:" + fileName);
            writer.flush();
            buff= new char[16*1024];
            buffSize = reader.read(buff);
            String temp = String.copyValueOf(buff, 0, 5);
            if(temp.equalsIgnoreCase("@200:")){
                String file[] = String.valueOf(buff, 5, buffSize-5).split("\\|");
                if(file[0].equalsIgnoreCase(filemd5)){
                    return Long.valueOf(file[1]);
                }
                else{
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@501:"))){
                        buff= new char[16*1024];
                        buffSize = reader.read(buff);
                        writer.write("@500:", 0, 5);
                        writer.flush();
                    }
                    System.out.println("Close at 170");
                    return -1;
                }
            }
            else if(temp.equalsIgnoreCase("@404:")){
                System.out.println("File " + fileName + " not found.");
                return -1;
            }
            else{
                while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                    buff= new char[16*1024];
                    buffSize = reader.read(buff);
                    writer.write("@END:", 0, 5);
                    writer.flush();
                }
                socketOfClient.close();
                return -1;
            }
        } catch(ConnectException e){
            System.err.printf("Connect to %s:%d fail\n",socketOfClient.getInetAddress().getHostAddress(), socketOfClient.getPort());
            return -2;
        } catch(SocketException e){
            System.err.println("Socket Error: " + e);
            return -2;
        } catch(IOException e){
            System.err.println("IOException!");
            return -2;
        }
    }
    
    private static long getFileSize(char[] buff,int buffSize){
        String temp = String.copyValueOf(buff, 0, 5);
        if(temp.equalsIgnoreCase("@200:")){
            return Long.parseLong(String.valueOf(buff, 7, buffSize-7));
        }
        else if(temp.equalsIgnoreCase("@403:")){
            return -1;
        }
        else if(temp.equalsIgnoreCase("@404:")){
            return 0;
        }
        return -2;
    }
    
    private static long receiveFile(Socket sockConn,File fts, long fileSize){
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            fts.createNewFile();
            RandomAccessFile fwriter = new RandomAccessFile(fts, "rw");
            byte buff[] = new byte[16*1024];
            long buffSize = 16*1024;
            long fremain = fileSize;
            long foffset = 0;
            int receivedB;
            long start = System.currentTimeMillis();
            while(fremain>0){
                buff = new byte[16*1024];
                receivedB = dis.read(buff);
                if((int)buff[0] != 49){
                    break;
                }
                else{
                    foffset = Long.valueOf(new String(Arrays.copyOfRange(buff, 1, 21)));
                    fwriter.seek(foffset);
                    fwriter.write(Arrays.copyOfRange(buff, 21, receivedB), 0, receivedB-21);
                    buff = longToBytes(foffset);
                    dos.write(buff,0,buff.length);
                    dos.flush();
                    fremain=(fileSize-foffset-receivedB+21);
                }
                System.out.printf("\r%.2f %%",(float)(foffset+receivedB-21)*100.0/(float)fileSize);
            }
            long duration = System.currentTimeMillis() - start;
            if(duration<1000){
                System.out.print(" (" + duration + " ms)");
            }
            else{
                System.out.printf(" (%.2f s)", (float)duration/1000.0);
            }
            return fileSize-fremain;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.put(bytes);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
}

class Piece {
    public long offset,count;
    public boolean done = false;
    public boolean downloading = false;

    public Piece(long offset, long count) {
        this.offset = offset;
        this.count = count;
    }
}

class DownloadPiece extends Thread{
    public String IP;
    public String fileName,md5;
    public long totalReceived;
    public long fileSize; 
    public Work work;
    public int attempt = 0;
    public long speed = 0;

    public DownloadPiece(String IP, String fileName, String md5, long fileSize, Work work) {
        this.IP = IP;
        this.fileName = fileName;
        this.md5 = md5;
        this.fileSize = fileSize;
        this.work = work;
    }
    
    public void run(){
        while(!work.isAllDone()){
            try {
                this.attempt++;
                Socket sock = new Socket(IP,12345);
                totalReceived = downloadFromClient(sock, work, fileName, md5, fileSize);
            } catch (IOException ex) {
                System.out.println("Error when connect to IP " + IP + " after " + attempt + " attempt(s)");
                if(!work.isAllDone()){
                    try {
                        sleep(attempt*5000);
                    } catch (InterruptedException ex1) {
                        //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }
        }
        System.out.println("Thread download file " + fileName + " with IP " + IP + " stopped.");
    }
    
    public long downloadFromClient(Socket sock, Work work, String fileName, String md5, long fileSize){
        long output = 0;
        try {
            long checkFileSize = fileChecker(fileName, md5, sock);
            if(fileSize==checkFileSize){
                File dir = new File(fileName.split("\\.")[0]);
                dir.mkdir();
                System.out.println("File checked OK. Starting download... ");
                while(!work.isAllDone()){
                    long[] temp = work.getWork();
                    if(temp[1] != 0){
                        File fts = new File(fileName.split("\\.")[0] + "/" + fileName.split("\\.")[0] + "-" + temp[0] + "-" + temp[1] + "-" + fileName.split("\\.")[1] + "." + "CAPSLOCKTEAM");
                        fts.createNewFile();
                        long received = receiveFile(sock, fts, temp[0], temp[1]);
                        if(received == -1){
                            work.reWork(temp[0], temp[1]);
                        }
                        else if(received == 0){
                            fts.delete();
                            work.reWork(temp[0], temp[1]);
                        }
                        else if(received != temp[1]){
                            fts.renameTo(new File(fileName.split("\\.")[0] + "-" + temp[0] + "-" + received + "." + fileName.split("\\.")[1]));
                            work.doneWork(temp[0],temp[1]);
                            long last = temp[1] - received;
                            long newoffset = temp[0] + received;
                            work.addWork(newoffset, last);
                            output += received;
                        }
                        else{
                            work.doneWork(temp[0], temp[1]);
                            output += received;
                        }
                    }
                }
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
                BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                char[] buff = new char[16*1024];
                int buffSize = 16*1024;
                while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                    writer.write("@END:", 0, 5);
                    writer.flush();
                    buff = new char[16*1024];
                    buffSize = reader.read(buff);                    
                }
                sock.close();
                return output;
            }
            else {
                System.out.println("File check fail from: " + sock.getInetAddress());
                return -1;
            }
        } catch (IOException ex) {
            //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error on thread " + IP + " | "+ ex);
            return -1;
        }
    }
    
    private long receiveFile(Socket sockConn, File fts, long offset, long count){
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sockConn.getOutputStream()));
            writer.write("@101:" + String.valueOf(offset) + '|' + String.valueOf(count));
            writer.flush();
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            fts.createNewFile();
            RandomAccessFile fwriter = new RandomAccessFile(fts, "rw");
            byte buff[] = new byte[16*1024];
            long buffSize = 16*1024;
            long fremain = count;
            long foffset = 0;
            int receivedB;
//            long start = System.currentTimeMillis();
            while(fremain>0){
                long speedCal = fremain;
                buff = new byte[16*1024];
                receivedB = dis.read(buff);
                if((int)buff[0] != 49){
                    break;
                }
                else{
                    foffset = Long.valueOf(new String(Arrays.copyOfRange(buff, 1, 21)));
                    fwriter.seek(foffset-offset);
                    fwriter.write(Arrays.copyOfRange(buff, 21, receivedB), 0, receivedB-21);
                    buff = longToBytes(foffset);
                    dos.write(buff,0,buff.length);
                    dos.flush();
                    fremain=(count-(foffset-offset)-receivedB+21);
                    speed += (speedCal - fremain);
                }
                //System.out.printf("\rReceiving file (%s): %.2f %%", fts.getName(), (float)(foffset-offset+receivedB-21)*100.0/(float)count);
            }
//            long duration = System.currentTimeMillis() - start;
//            if(duration<1000){
//                System.out.println(" (" + duration + " ms)");
//            }
//            else{
//                System.out.printf(" (%.2f s)\n", (float)duration/1000.0);
//            }
            fwriter.close();
            return count-fremain;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error at line 422");
        }
        return 0;
    }
    
    public long fileChecker(String fileName, String filemd5, Socket socketOfClient) {
        BufferedWriter writer;
        BufferedReader reader;
        char buff[] = new char[16*1024];
        int buffSize;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
            writer.write("@100:" + fileName,0,5 + fileName.length());
            writer.flush();
            buff= new char[16*1024];
            buffSize = reader.read(buff);
            String temp = String.copyValueOf(buff, 0, 5);
            if(temp.equalsIgnoreCase("@200:")){
                String file[] = String.valueOf(buff, 5, buffSize-5).split("\\|");
                if(file[0].equalsIgnoreCase(filemd5)){
                    return Long.valueOf(file[1]);
                }
                else{
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@501:"))){
                        buff= new char[16*1024];
                        buffSize = reader.read(buff);
                        writer.write("@500:", 0, 5);
                        writer.flush();
                    }
                    socketOfClient.close();
                    return -1;
                }
            }
            else if(temp.equalsIgnoreCase("@404:")){
                System.out.println("File " + fileName + " not found.");
                return -1;
            }
            else{
                while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                    buff= new char[16*1024];
                    buffSize = reader.read(buff);
                    writer.write("@END:", 0, 5);
                    writer.flush();
                }
                socketOfClient.close();
                return -1;
            }
        } catch(ConnectException e){
            System.err.printf("Connect to %s:%d fail\n",socketOfClient.getInetAddress().getHostAddress(), socketOfClient.getPort());
            return -2;
        } catch(SocketException e){
            System.err.println("Socket Error: " + e);
            return -2;
        } catch(IOException e){
            System.err.println("IOException!");
            return -2;
        }
    }
}

class Work{
    String fileName;
    long fileSize;
    String filemd5;
    ArrayList<Piece> downloadList;

    public Work(String fileName, long fileSize, String filemd5, ArrayList<Piece> downloadList) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filemd5 = filemd5;
        this.downloadList = downloadList;
    }
    
    public boolean isAllDone(){
        for(Piece ele : downloadList){
            if(ele.done == false) return false;
        }
        return true;
    }
    
    public long[] getWork(){
        long[] output = {0,0};
        if(!isAllDone()){
            for(Piece ele : downloadList){
                if(ele.downloading == false && ele.done == false){
                    output[0] = ele.offset;
                    output[1] = ele.count;
                    ele.downloading = true;
                    return output;
                }
            }
        }
        return output;
    }
    
    public void doneWork(long offset, long count){
        for(Piece ele : downloadList){
            if(ele.offset == offset && ele.count == count){
                ele.done = true;
                ele.downloading = false;
            }
        }
    }
    
    public void addWork(long offset, long count){
        downloadList.add(new Piece(offset, count));
    }
    
    public void reWork(long offset, long count){
        for(Piece ele : downloadList){
            if(ele.offset == offset && ele.count == count){
                ele.done = false;
                ele.downloading = false;
            }
        }
    }
}

class speedTest extends Thread{
    DownloadPiece piece;

    public speedTest(DownloadPiece piece) {
        this.piece = piece;
    }

    public void run(){
        while(piece.isAlive()){
            try {
                piece.speed = 0;
                sleep(500);
                if((piece.speed)>1048576 && piece.isAlive()){
                    System.out.printf("\rSpeed of %s: %.2f MB/s", piece.IP, (float) (piece.speed)/1048576*2);
                }
                else if((piece.speed)>1024 && piece.isAlive()){
                    System.out.printf("\rSpeed of %s: %.2f KB/s", piece.IP, (float) (piece.speed)/1024*2);
                }
                else if((piece.speed)>0 && piece.isAlive()){
                    System.out.printf("\rSpeed of %s: %.2f B/s", piece.IP, (float) (piece.speed)*2);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(speedTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}