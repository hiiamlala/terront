
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import static java.lang.Thread.sleep;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;

/**
 *
 * @author hiiamlala
 */
public class Terront {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Server server = new Server();
//        server.start();
        Client client = new Client();
        client.start();
    }
}

class Client extends Thread{
    public void run(){
        File downDir = new File("Download/");
        downDir.mkdir();
        String[] IPList = {"35.240.249.118","115.126.25.1","35.235.110.247"};
//        String[] IPList = {"127.0.0.1","115.126.25.1","15.25.33.45"};
        String fileName = "3.jpg";
        String filemd5 = "973b31c1346a14c900d15679e8a0a980";
        long fileSize = 5168810;
        //long fileSize = 410865392;
        Download down = new Download(IPList, fileName, filemd5, fileSize);
        down.start();
    }
}

class Download extends Thread{
    public String[] IPList;
    public long fileSize;
    public String fileName,filemd5;
    public ArrayList<DownloadPiece> PieceList = new ArrayList<>();
    public ArrayList<speedTest> speedList = new ArrayList<>();
    public int piececount = 0;
    public Work work;

    public Download(String[] IPList, String fileName, String filemd5, long fileSize) {
        this.IPList = IPList;
        this.fileName = fileName;
        this.filemd5 = filemd5;
        this.fileSize = fileSize;
        this.work = new Work(fileName, fileSize, filemd5, generateDownloadList(fileSize,IPList.length));
    }
    
    public void run(){
        int i = 0;
        long fileSizeChecked=0;
        long start = System.currentTimeMillis();
        File output = new File("Download/" + fileName);
        for(String element : IPList){
            System.out.println("Start thread with IP: " + element);
            PieceList.add(new DownloadPiece(element, fileName, filemd5, fileSize, work));
            speedList.add(new speedTest(PieceList.get(piececount)));
            PieceList.get(piececount).start();
            speedList.get(piececount).start();
            piececount++;
        }
        Progress prog = new Progress(work, PieceList);
        prog.run();
        while(!work.isAllDone()){
            try {
                sleep(500);
            } catch (InterruptedException ex) {
                //Logger.getLogger(Download.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        long duration = System.currentTimeMillis() - start;
        if(duration<1000){
            System.out.print("\nDowload in: (" + duration + " ms)\n");
        }
        else{
            System.out.printf("Download in: (%.2f s)\n", (float)duration/1000.0);
        }
        mergeFile(fileName, output);
        duration = System.currentTimeMillis() - start;
        if(duration<1000){
            System.out.print("\nTotal: (" + duration + " ms)\n");
        }
        else{
            System.out.printf("Total: (%.2f s)\n", (float)duration/1000.0);
        }
    }
    
    public static boolean mergeFile(String fileName, File output){
        try {
            File dir = new File("temp/" + fileName.split("\\.")[0]);
            File[] list = dir.listFiles();
            output.createNewFile();
            RandomAccessFile fwriter = new RandomAccessFile(output, "rw");
            for(File ele : list){
                if(ele.getName().contains("CAPSLOCKTEAM")){
                    String[] detail = ele.getName().split("\\-");
                    if(detail[0].equalsIgnoreCase(fileName.split("\\.")[0])){
                        long offset = Long.valueOf(detail[detail.length - 3]);
                        long count = Long.valueOf(detail[detail.length - 2]);
                        byte[] buff = new byte[(int)count];
                        FileInputStream reader = new FileInputStream(ele);
                        reader.read(buff,0,(int)count);
                        fwriter.seek(offset);
                        fwriter.write(buff);
                        reader.close();
                        ele.delete();
                    }
                }
            }
            dir.delete();
            fwriter.close();
            return true;
        } catch (IOException ex) {
            System.out.println("Error when save file: " + fileName);
            return false;
        }
    }
    
    public static ArrayList<Piece> generateDownloadList(long fileSize, int hostNumber){
        ArrayList<Piece> output = new ArrayList<>();
        if(hostNumber <= 1){
            output.add(new Piece(0, fileSize));
        }
        else{
            int devide;
            if(fileSize > (int) Math.pow(hostNumber, 2)){
                devide = (int) Math.pow(hostNumber, 2);
            }
            else devide = hostNumber;
            long interval = fileSize/devide;
            for(int i = 0; i<devide; i++){
                if(i==devide-1){
                    output.add(new Piece(i*interval, fileSize - i*interval));
                }
                else    output.add(new Piece(i*interval, interval));
                
            }
        }
        return output;
    }
    
    public long fileChecker(String fileName, String filemd5, Socket socketOfClient) {
        BufferedWriter writer;
        BufferedReader reader;
        char buff[] = new char[16*1024];
        int buffSize;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
            writer.write(fillBuff("@100:" + fileName.length() + "|" + fileName,16*1024),0, 16*1024);
            writer.flush();
            buff= new char[16*1024];
            buffSize = reader.read(buff);
            String temp = String.copyValueOf(buff, 0, 5);
            if(temp.equalsIgnoreCase("@200:")){
                String file[] = String.valueOf(buff, 5, buffSize-5).split("\\|");
                if(file[0].equalsIgnoreCase(filemd5)){
                    return Long.valueOf(file[1]);
                }
                else{
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@501:"))){
                        buff= new char[16*1024];
                        buffSize = reader.read(buff);
                        writer.write(fillBuff("@500:",16*1024), 0, 16*1024);
                        writer.flush();
                    }
                    System.out.println("Close at 170");
                    return -1;
                }
            }
            else if(temp.equalsIgnoreCase("@404:")){
                System.out.println("File " + fileName + " not found.");
                return -1;
            }
            else{
                while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                    buff= new char[16*1024];
                    buffSize = reader.read(buff);
                    writer.write(fillBuff("@END:",16*1024), 0, 16*1024);
                    writer.flush();
                }
                socketOfClient.close();
                return -1;
            }
        } catch(ConnectException e){
            System.err.printf("Connect to %s:%d fail\n",socketOfClient.getInetAddress().getHostAddress(), socketOfClient.getPort());
            return -2;
        } catch(SocketException e){
            System.err.println("Socket Error: " + e);
            return -2;
        } catch(IOException e){
            System.err.println("IOException!");
            return -2;
        }
    }
    
    private static long getFileSize(char[] buff,int buffSize){
        String temp = String.copyValueOf(buff, 0, 5);
        if(temp.equalsIgnoreCase("@200:")){
            return Long.parseLong(String.valueOf(buff, 7, buffSize-7));
        }
        else if(temp.equalsIgnoreCase("@403:")){
            return -1;
        }
        else if(temp.equalsIgnoreCase("@404:")){
            return 0;
        }
        return -2;
    }
    
    private static long receiveFile(Socket sockConn,File fts, long fileSize){
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            fts.createNewFile();
            RandomAccessFile fwriter = new RandomAccessFile(fts, "rw");
            byte buff[] = new byte[16*1024];
            long buffSize = 16*1024;
            long fremain = fileSize;
            long foffset = 0;
            int receivedB = (int)buffSize;
            long start = System.currentTimeMillis();
            while(fremain>0){
                buff = new byte[16*1024];
                dis.readFully(buff,0,16*1024);
                if((int)buff[0] != 49){
                    break;
                }
                else{
                    foffset = Long.valueOf(new String(Arrays.copyOfRange(buff, 1, 21)));
                    fwriter.seek(foffset);
                    fwriter.write(Arrays.copyOfRange(buff, 21, receivedB), 0, receivedB-21);
                    buff = Download.longToBytes(foffset);
                    dos.write(buff,0,buff.length);
                    dos.flush();
                    fremain=(fileSize-foffset-receivedB+21);
                }
                //System.out.printf("\r%.2f %%",(float)(foffset+receivedB-21)*100.0/(float)fileSize);
            }
            long duration = System.currentTimeMillis() - start;
            if(duration<1000){
                System.out.print(" (" + duration + " ms)");
            }
            else{
                System.out.printf(" (%.2f s)", (float)duration/1000.0);
            }
            return fileSize-fremain;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.put(bytes);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
    
    public String fillBuff(String src, int buffSize){
        String output = src;
        while(output.length()<buffSize){
            output+="0";
        }
        return output;
    }
}

class Piece {
    public long offset,count;
    public boolean done = false;
    public boolean downloading = false;

    public Piece(long offset, long count) {
        this.offset = offset;
        this.count = count;
    }
}

class DownloadPiece extends Thread{
    public String IP;
    public String fileName,md5;
    public long totalReceived;
    public long fileSize; 
    public Work work;
    public int attempt = 0;
    public long speedCal = 0;
    public double progress = 0;
    public long count;
    public long speed = 0;
    

    public DownloadPiece(String IP, String fileName, String md5, long fileSize, Work work) {
        this.IP = IP;
        this.fileName = fileName;
        this.md5 = md5;
        this.fileSize = fileSize;
        this.work = work;
    }
    
    public void run(){
        while(!work.isAllDone()){
            totalReceived = downloadFromClient(IP, work, fileName, md5, fileSize);
            if(totalReceived == -1){
                break;
            }
        }
        System.out.println("Thread download file " + fileName + " with IP " + IP + " stopped.");
    }
    
    public long downloadFromClient(String IP, Work work, String fileName, String md5, long fileSize){
        Socket sock;
        attempt++;
        try {
            sock = new Socket(IP,12345);
            long output = 0;
            try {
                long checkFileSize = fileChecker(fileName, md5, sock);
                if(fileSize==checkFileSize){
                    File dir = new File("temp/" + fileName.split("\\.")[0]);
                    dir.mkdir();
                    System.out.println("File checked OK. Starting download... ");
                    while(!work.isAllDone()){
                        long[] temp = work.getWork();
                        count = temp[1];
                        if(temp[1] != 0){
                            File fts = new File("temp/" + fileName.split("\\.")[0] + "/" + fileName.split("\\.")[0] + "-" + temp[0] + "-" + temp[1] + "-" + fileName.split("\\.")[1] + "." + "CAPSLOCKTEAM");
                            fts.createNewFile();
                            long received = receiveFile(sock, fts, temp[0], temp[1]);
                            if(received == -1){
                                work.reWork(temp[0], temp[1]);
                                return -1;
                            }
                            else if(received == 0){
                                fts.delete();
                                work.reWork(temp[0], temp[1]);
                                try {
                                    attempt++;
                                    sleep(attempt*2000);
                                    return 0;
                                } catch (InterruptedException ex) {
                                    //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            else if(received < temp[1]){
                                fts.renameTo(new File("temp/" + fileName.split("\\.")[0]+ "/" + fileName.split("\\.")[0] + "-" + temp[0] + "-" + received + "-" + fileName.split("\\.")[1] + "." + "CAPSLOCKTEAM"));
                                fts.delete();
                                work.doneWork(temp[0],temp[1]);
                                long last = temp[1] - received;
                                long newoffset = temp[0] + received;
                                work.addWork(newoffset, last);
                                output += received;
                                attempt++;
                                try {
                                    sleep(attempt*2000);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            else if(received == temp[1]){
                                attempt = 0;
                                work.doneWork(temp[0], temp[1]);
                                output += received;
                            }
                            else{
                                attempt++;
                                try {
                                    sleep(attempt*2000);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                fts.delete();
                                work.reWork(temp[0], temp[1]);
                                return 0;
                            }
                        }
                        else {
                            try {
                                sleep(500);
                            } catch (InterruptedException ex) {
                                //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    char[] buff = new char[16*1024];
                    int buffSize = 16*1024;
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                        writer.write(fillBuff("@END:",16*1024), 0, 16*1024);
                        writer.flush();
                        buff = new char[16*1024];
                        buffSize = reader.read(buff);
                    }
                    sock.close();
                    return output;
                }
                else {
                    System.out.println("File check fail from: " + sock.getInetAddress());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    char[] buff = new char[16*1024];
                    int buffSize = 16*1024;
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                        writer.write(fillBuff("@END:",16*1024), 0, 16*1024);
                        writer.flush();                    
                        buff = new char[16*1024];
                        buffSize = reader.read(buff);
                    }
                    sock.close();
                    return -1;
                }
            } catch (IOException ex) {
                //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Error on thread " + IP + " | "+ ex);
                return -1;
            }
        } catch (IOException ex) {
            System.out.println("Error when connect to IP " + IP + " after " + attempt + " attempt(s)");
            if(!work.isAllDone()){
                try {
                    sleep(attempt*5000);
                    return 0;
                } catch (InterruptedException ex1) {
                    //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex1);
                    return 0;
                }
            }
            return 0;
        }
    }
    
    private long receiveFile(Socket sockConn, File fts, long offset, long count){
    long fremain = count;
    RandomAccessFile fwriter;
        try {
            fwriter = new RandomAccessFile(fts, "rw");
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(sockConn.getOutputStream()));
            writer.write(fillBuff("@101:" + (new String(String.valueOf(offset) + '|' + String.valueOf(count))).length() + "|" + String.valueOf(offset) + '|' + String.valueOf(count), 16*1024),0,16*1024);
            writer.flush();
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            fts.createNewFile();
            byte buff[] = new byte[16*1024];
            byte temp[] = new byte[16*1024];
            int buffSize = 16*1024;
            long foffset = 0;
            int receivedB = buffSize;
            fwriter.seek(0);
//            long start = System.currentTimeMillis();
            while(fremain>0){
                buff = new byte[16*1024];
                dis.readFully(buff,0,16*1024);
                if(fremain>receivedB){
                    fwriter.write(buff,0,receivedB);
                    fremain-= receivedB;
                    speedCal += receivedB;
                }
                else{
                    fwriter.write(buff,0,(int)fremain);
                    fremain-= fremain;
                    speedCal += fremain;
                }
                progress = (float)(count-fremain)*100.0/(float)count;
                //System.out.printf("\r%.2f %%", (float)(count-fremain)*100.0/(float)count);
            }
//            long duration = System.currentTimeMillis() - start;
//            if(duration<1000){
//                System.out.println(" (" + duration + " ms)");
//            }
//            else{
//                System.out.printf(" (%.2f s)\n", (float)duration/1000.0);
//            }
            fwriter.close();
            return count-fremain;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex) {
            System.out.println(ex);
            return -1;
        } catch (EOFException ex){
        try {
            System.out.println(this.IP + " disconnected. Return " + (count-fremain));
            fwriter.close();
            sockConn = new Socket(IP,12345);
            return count-fremain;
        } catch (IOException ex1) {
            //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex1);
            return 0;
        }
        } catch (SocketException ex){
            try {
                System.out.println(this.IP + " disconnected. Return " + (count-fremain));
                fwriter.close();
                sockConn = new Socket(IP,12345);
                return count-fremain;
            } catch (IOException ex1) {
                //Logger.getLogger(DownloadPiece.class.getName()).log(Level.SEVERE, null, ex1);
                return 0;
            }
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error at line 462");
            return -1;
        }
        return 0;
    }
    
    public long fileChecker(String fileName, String filemd5, Socket socketOfClient) {
        BufferedWriter writer;
        BufferedReader reader;
        char buff[] = new char[16*1024];
        int buffSize;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
            writer.write(fillBuff("@100:" + fileName.length() + "|" + fileName,16*1024),0,16*1024);
            writer.flush();
            buff= new char[16*1024];
            buffSize = reader.read(buff);
            String temp = String.copyValueOf(buff, 0, 5);
            if(temp.equalsIgnoreCase("@200:")){
                String file[] = String.valueOf(buff, 5, buffSize-5).split("\\|");
                if(file[0].equalsIgnoreCase(filemd5)){
                    return Long.valueOf(file[1]);
                }
                else{
                    while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@501:"))){
                        buff= new char[16*1024];
                        buffSize = reader.read(buff);
                        writer.write(fillBuff("@500:",16*1024), 0, 16*1024);
                        writer.flush();
                    }
                    socketOfClient.close();
                    return -1;
                }
            }
            else if(temp.equalsIgnoreCase("@404:")){
                System.out.println("File " + fileName + " not found.");
                return -1;
            }
            else{
                while(buffSize>0 && !(new String(buff,0,8).equalsIgnoreCase("@CLOSED:"))){
                    buff= new char[16*1024];
                    buffSize = reader.read(buff);
                    writer.write(fillBuff("@END:",16*1024), 0, 16*1024);
                    writer.flush();
                }
                socketOfClient.close();
                return -1;
            }
        } catch(ConnectException e){
            System.err.printf("Connect to %s:%d fail\n",socketOfClient.getInetAddress().getHostAddress(), socketOfClient.getPort());
            return -2;
        } catch(SocketException e){
            System.err.println("Socket Error: " + e);
            return -2;
        } catch(IOException e){
            System.err.println("IOException!");
            return -2;
        }
    }
    
    private static void arraysJoin(byte[] src, byte[] des, int offset, int length){
        for(int i=0;i<length;i++){
            des[offset+i] = src[i];
        }
    }
    
    public String fillBuff(String src, int buffSize){
        String output = src;
        while(output.length()<buffSize){
            output+="0";
        }
        return output;
    }
}

class Work{
    String fileName;
    long fileSize;
    String filemd5;
    ArrayList<Piece> downloadList;

    public Work(String fileName, long fileSize, String filemd5, ArrayList<Piece> downloadList) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filemd5 = filemd5;
        this.downloadList = downloadList;
    }
    
    public boolean isAllDone(){
        for(Piece ele : downloadList){
            if(ele.done == false) return false;
        }
        return true;
    }
    
    public long[] getWork(){
        long[] output = {0,0};
        if(!isAllDone()){
            for(Piece ele : downloadList){
                if(ele.downloading == false && ele.done == false){
                    output[0] = ele.offset;
                    output[1] = ele.count;
                    ele.downloading = true;
                    return output;
                }
            }
        }
        return output;
    }
    
    public void doneWork(long offset, long count){
        for(Piece ele : downloadList){
            if(ele.offset == offset && ele.count == count){
                ele.done = true;
                ele.downloading = false;
            }
        }
    }
    
    public void addWork(long offset, long count){
        downloadList.add(new Piece(offset, count));
    }
    
    public void reWork(long offset, long count){
        for(Piece ele : downloadList){
            if(ele.offset == offset && ele.count == count){
                ele.done = false;
                ele.downloading = false;
            }
        }
    }
}

class speedTest extends Thread{
    DownloadPiece piece;

    public speedTest(DownloadPiece piece) {
        this.piece = piece;
    }

    public void run(){
        while(piece.isAlive()){
            try {
                piece.speedCal = 0;
                sleep(1000);
                piece.speed = (long) Math.floor(piece.speedCal*(1-0.05)+piece.speed*0.05);
            } catch (InterruptedException ex) {
                Logger.getLogger(speedTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

class Server extends Thread{
    public static int downloaded=0;
    /**
     * @param args the command line arguments
     */
    
    public void run() {
        ServerSocket listener = null;
        try {
            listener = new ServerSocket(12345);
            System.out.printf("Waiting for connection on port %d ...",listener.getLocalPort());
        } catch (IOException e){
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, e);
        }
        while(true){
            try {
                Socket newSock = listener.accept();
                Thread serverServe = new ServerServe(newSock);
                serverServe.start();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }        
        }
    }
}

class ServerServe extends Thread{
    private Socket newSock;
    public ServerServe(Socket connSock) {
        this.newSock = connSock;
   }
    public void run()  {
        try {
            BufferedReader reader;
            BufferedWriter writer;
            String fileName;
            char[] buff = new char[16*1024];
            int buffSize;
            System.out.printf("\nConnected(%s:%d)\n",newSock.getInetAddress(),newSock.getPort());
            reader = new BufferedReader(new InputStreamReader(newSock.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(newSock.getOutputStream()));
            buffSize = reader.read(buff);
            if(new String(buff,0,5).equalsIgnoreCase("@100:")){
                String[] temp = new String(buff,5,buffSize-5).split("\\|");
                fileName = new String(temp[1].toCharArray(),0, Integer.valueOf(temp[0]));
                String md5 = "ERROR";
//                try (InputStream is = new FileInputStream(new File(fileName))) {
//                    md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
//                    is.close();
//                } catch(FileNotFoundException ex){
//                    buff = new String("@404:").toCharArray();
//                    writer.write(buff, 0, buff.length);
//                    writer.flush();
//                }
                long fileSize = new File(fileName).length();
                buff = new String("@200:" + "973b31c1346a14c900d15679e8a0a980" + '|' + fileSize).toCharArray();
                writer.write(buff, 0, buff.length);
                writer.flush();
                while(true){
                    buff = new char[16*1024];
                    buffSize = reader.read(buff);
                    if(new String(buff,0,5).equalsIgnoreCase("@101:")){
                        temp = new String(buff,5,buffSize-5).split("\\|");
                        long offset = Long.parseLong(temp[1]);
                        long count = Long.parseLong(new String(temp[2].toCharArray(),0,Integer.valueOf(temp[0])-temp[1].length()-1));
                        sendFile(newSock, new File(fileName), offset, count);
                    }
                    else if(new String(buff,0,5).equalsIgnoreCase("@END:")){
                        System.out.println("\nServer Received @END: from " + newSock.getInetAddress());
                        writer.write(fillBuff("@CLOSED:",16*1024), 0, 16*1024);
                        writer.flush();
                        break;
                    }
                }
            }
            else{
                //Not a req
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            try {
                newSock.close();
                System.out.printf("\nClosed %s:%d\n",newSock.getInetAddress(),newSock.getPort());
            } catch (IOException ex) {
                Logger.getLogger(ServerServe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static long sendFile(Socket sockConn, File fts){
        RandomAccessFile freader = null;
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            freader = new RandomAccessFile(fts, "r");
            byte buff[] = new byte[16*1024];
            byte temp[] = new byte[16*1024];
            long fileSize = fts.length();
            long buffSize = 16*1024;
            long fremain = fileSize;
            long foffset = 0;
            long sendB = buffSize-21;
            int responseSize;
            long start = System.currentTimeMillis();
            while(fremain>0){
                if(fremain<=sendB){
                    sendB=fremain;
                }
                buff = new byte[16*1024];
                freader.seek(foffset);
                freader.read(buff,21,Integer.parseInt(String.valueOf(sendB)));
                arraysFill(offsetString(foffset).getBytes(),buff, 0, 21);
                dos.write(buff, 0, Integer.parseInt(String.valueOf(sendB))+21);
                dos.flush();
                responseSize = 0;
                while(responseSize<(16*1024)){
                    temp = new byte[16*1024];
                    int tempBuffSize = dis.read(temp);
                    arraysJoin(temp , buff, responseSize, tempBuffSize);
                    responseSize+=tempBuffSize;
                }
                if(new String(buff).equalsIgnoreCase("@END:")){
                    break;
                }
                else if(bytesToLong(buff)>fileSize){
                    break;
                }
                else if(foffset==bytesToLong(buff)){
                    foffset += sendB;
                    fremain -= sendB;
                }
                System.out.printf("\r%.2f %%",(float)foffset*100.0/(float)fileSize);
            }
            long duration = System.currentTimeMillis()-start;
            if(duration<1000){
                System.out.print(" (" + duration + " ms)");
            }
            else{
                System.out.printf(" (%.2f s)", (float)duration/1000.0);
            }
            return foffset;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                freader.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }
    
    private static long sendFile(Socket sockConn, File fts, long offset, long count){
        RandomAccessFile freader = null;
        try {
            DataOutputStream dos = new DataOutputStream(sockConn.getOutputStream());
            DataInputStream dis = new DataInputStream(sockConn.getInputStream());
            freader = new RandomAccessFile(fts, "r");
            byte buff[] = new byte[16*1024];
            long fileSize = fts.length();
            long buffSize = 16*1024;
            long fremain = count;
            long foffset = offset;
            int sendB = 16*1024;
            int responseSize;
//            long start = System.currentTimeMillis();
            while(fremain>0){
                buff = new byte[16*1024];
                freader.seek(foffset);
                freader.read(buff,0,(int) buffSize);
                dos.write(buff, 0, sendB);
                dos.flush();
                foffset+=sendB;
                fremain-=sendB;
            }
//            long duration = System.currentTimeMillis()-start;
//            if(duration<1000){
//                System.out.printf(" (" + duration + " ms)");
//            }
//            else{
//                System.out.printf(" (%.2f s)", (float)duration/1000.0);
//            }
            freader.close();
            return foffset;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                freader.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }
    
    private static String offsetString(long foffset){
        String temp = String.valueOf(foffset);
        String output = "1";
        for(int i = 0; i < 20-temp.length();i++){
            output += "0";
        }
        output+=temp;
        return output;
    }
    
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(16*1024);
        buffer.put(bytes);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
    
    private static void arraysFill(byte[] src, byte[] des, int offset, int length){
        for(int i=offset;i<offset+length;i++){
            des[i] = src[i];
        }
    }
    
    private static void arraysJoin(byte[] src, byte[] des, int offset, int length) {
        for(int i=0;i<length;i++){
            des[offset+i] = src[i];
        }
    }
    
    public String fillBuff(String src, int buffSize){
        String output = src;
        while(output.length()<buffSize){
            output+="0";
        }
        return output;
    }
}

class Progress extends Thread{
    Work work;
    ArrayList<DownloadPiece> PieceList;

    public Progress(Work work, ArrayList<DownloadPiece> PieceList) {
        this.work = work;
        this.PieceList = PieceList;
    }

    @Override
    public void run(){
        float lastProgress = 0;
        float lastEstTime = 0;
        long lastSpeed = 0;
        while(!work.isAllDone()){
            try {
                float progress = 0;
                long speed = 0;
                for(Piece ele : work.downloadList){
                    if(ele.done == true){
                        progress += ((float) ele.count)*100/(float)work.fileSize;
                    }
                }
                for(DownloadPiece ele : PieceList){
                    progress+=ele.progress*((float) ele.count)/(float)work.fileSize;
                    speed+=ele.speed;
                }
                float estTime = (100-progress)/(progress-lastProgress);
                speed = (long) Math.floor(speed*(1-0.05)+lastSpeed*0.05);
                if(speed>1024*1024){
                    System.out.println("Progress: " + progress + "% Speed: " + (float) speed/1024/1024 + "MB/s Estimated time: " + (estTime*(1-0.05)+lastEstTime*0.05) + "s \n");
                }
                else if(speed > 1024){
                    System.out.println("Progress: " + progress + "% Speed: " + (float) speed/1024 + "kB/s Estimated time: " + (estTime*(1-0.05)+lastEstTime*0.05) + "s \n");
                }
                else    System.out.println("Progress: " + progress + "% Speed: " + speed + "B/s Estimated time: " + (estTime*(1-0.05)+lastEstTime*0.05) + "s \n");
                lastProgress = progress;
                lastSpeed = speed;
                if(estTime<1000000000) lastEstTime = estTime;
                sleep(2000);
            } catch (InterruptedException ex) {
                //Logger.getLogger(Progress.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}